let gulp = require('gulp');
let sass = require('gulp-sass');
let concat = require('gulp-concat');
let autoprefix = require('gulp-autoprefixer');
let cleancss = require('gulp-clean-css');
let uglify = require('gulp-uglify');
let source = require('gulp-sourcemaps');
let browserSync = require('browser-sync');

gulp.task('All', function () {
    gulp.start('sass-watch', 'Browser-Sync');
});

// Convert SASS
gulp.task('Compile style.scss', function () {
    gulp.src('./sass/style.scss')
        .pipe(source.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefix('last 4 versions'))
        .pipe(cleancss({compatibility: 'ie10'}))
        .pipe(concat('style.css'))
        .pipe(source.write('.'))
        .pipe(gulp.dest(''));
});

gulp.task('sass-watch', function () {
    gulp.watch('sass/**/*.scss', ['Compile style.scss']);
});

// Compile JS
gulp.task('JS Compile', function () {
    return gulp.src('dev/js/main.js')
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('production/js/'));
});

gulp.task('Browser-Sync', function () {
    browserSync.init({
        browser: "google chrome",
        proxy: "localhost:8000",
        notify: false
    });

    gulp.watch('./style.css').on('change', browserSync.reload);
    gulp.watch('./*.php').on('change', browserSync.reload);
});

