<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sar
 */

?>
<div class="blog-top">

    <div class="contributor">
        <div class="blog-title">
            <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
        </div>
        <img class="profile" src="<?php echo get_template_directory_uri(); ?>/assets/images/catriona_prof.jpg">
        <b>Catriona Valenta</b>
        <i>Contributor</i>
    </div>
    <div class="fi-container">
    <?php the_post_thumbnail([850, 490]); ?>
</div></div>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="blog-content">
        <?php
        the_content();

        ?>
    </div>

    <?php if (get_edit_post_link()) : ?>
        <footer class="entry-footer">
            <?php
            edit_post_link(
                sprintf(
                /* translators: %s: Name of current post */
                    esc_html__('Edit %s', 'sar'),
                    the_title('<span class="screen-reader-text">"', '"</span>', false)
                ),
                '<span class="edit-link">',
                '</span>'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-## -->
