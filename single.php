<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sar
 */

$showSidebar = true;
get_header(); ?>



            <?php
            while (have_posts()) : the_post();

                $category = get_the_category();

                if (($category[0]->cat_name) === "News") { ?>
                    <div id="primary" class="content-area news-size">
        <main id="main" class="site-main container" role="main"> <?php

                    // Blog Post - different styling
                    get_template_part('template-parts/content', get_post_format());
//                    the_post_navigation();
                    if (comments_open() || get_comments_number()) :
                        comments_template();
                    endif;


                } elseif ($category[0]->cat_name === "Blog") {

                    ?>
                    <div id="primary" class="content-area">
                    <main id="main" class="site-main container" role="main"> <?php

                    get_template_part('template-parts/content-blog', get_post_format());
                    the_post_navigation();
                    // If comments are open or we have at least one comment, load up the comment template.
                    if (comments_open() || get_comments_number()) :
                        comments_template();
                    endif;
                    $showSidebar = false;

                }

            endwhile; // End of the loop.
            ?>


        </main><!-- #main -->
    </div><!-- #primary -->

<?php
if ($showSidebar) {
    get_sidebar();
}
get_footer();
