<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 15/03/2018
 * Time: 19:36
 */


/**
 * The template for displaying the news page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sar
 */

// Number of posts to pull
$postCount = 6;


get_header(); ?>

<div id="primary" class="content-areasite news-size">
    <main id="main" class="site-main container news-styles" role="main">
        <?php

        // Start the loop.
        $wp_query = new WP_Query(array('posts_per_page' => $postCount));
        if ($wp_query->have_posts()) :
            while ($wp_query->have_posts()) : $wp_query->the_post();

                // Don't show blog posts in category
                if (in_category('News')) { ?>
                    <article id="post-<?php the_ID(); ?>" class="news-item-excerpt">
                        <div class="featured-image">
                        <?php the_post_thumbnail('small') ?>
                        </div>
                        <div class="snippet">
                            <h2><a href="<?php the_permalink(); ?>" class="news-link"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                        </div>
                    </article>
                    <?php
                }
            endwhile;             // End the loop
        endif;  // end have posts
        ?>


    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer(); ?>
