<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sar
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="site-info">
        <div class="container">
            <div class="left-footer">
                <ul>
                    <li><h3><a href="/privacy-policy">Privacy Policy</a></h3></li>
                    <li><h3><a href="/sitemap">Sitemap</a></h3></li>
                    <li><h3><a href="/contact">Contact Us</a></h3></li>
                </ul>
                <p class="sar">Sunrise Africa Relief is a registered charity in Scotland no. SC044899</p>
                <address class="charity-address">42 Swift Street, Dunfermline, Fife, KY11 8SN</address>
            </div>
            <div class="right-footer">
                <hr class="vertical"/>
                <div class="donation-radios">
                    <h3 class="donate">Donate now</h3>
                    <form id = "paypal-submit" target="_blank" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                        <ul>
                            <INPUT TYPE="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="src" value="1">
                            <input type="hidden" name="business"
                                   value="rwilliamson5@btinternet.com">
                            <INPUT TYPE="hidden" name="charset" value="utf-8">
                            <INPUT TYPE="hidden" NAME="return" value="http://www.sunriseafricarelief.com/thanks/">
                            <INPUT TYPE="hidden" NAME="currency_code" value="GBP">
                            <input type="hidden" name="hosted_button_id" value="Z6E6Q93AVKS3S"/>
                            <input type="hidden" name="image_url" VALUE="https://sunriseafricarelief.com/wp-content/uploads/2018/03/sar_paypal_logo.png">
                            <p>Make a quick donation via PayPal or card:</p>

                            <li>
                                <input type="submit" value="Donate">
                            </li>
                        </ul>
                    </form>

                </div>

            </div>
        </div><!-- .site-info -->
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
