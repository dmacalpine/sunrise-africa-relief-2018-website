jQuery(document).ready(function($) {

    // $(".js--btn").effect("shake", {times:2}, 50);



    if(typeof php_vars === 'undefined') {
        console.log("Could not load php_vars.")
    }

    else {
        var messageIndex = 2;
        window.setInterval(function () {
            switch (messageIndex) {

                case 1:
                    $('.js--recent-news-text').html("<a class='white-link' href='" + php_vars[0][1] + "'>" + php_vars[0][0] + "</a>");
                    break;

                case 2:
                    $('.js--recent-news-text').html("<a class='white-link' href='" + php_vars[1][1] + "'>" + php_vars[1][0] + "</a>");
                    break;

                case 3:
                    $('.js--recent-news-text').html("<a class='white-link' href='" + php_vars[2][1] + "'>" + php_vars[2][0] + "</a>");
                    break;
            }

            if (messageIndex === 3) {
                messageIndex = 1;
            }

            else {
                messageIndex++;
            }

        }, 5000);

    }
});

