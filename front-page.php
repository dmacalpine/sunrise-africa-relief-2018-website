<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sar
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main container" role="main">
            <div class="hero">
                <div class="hero-text">
                    <h1>Welcome</h1>
                    <p>Sunrise Africa Relief is an interfaith charity based in Dunfermline, Scotland dedicated to
                        alleviating poverty in the poorest areas in Africa and the UK.</p>
                    <p>Find out more about what we do and our latest projects.</p>
                    <a class="btn js--btn" href="javascript:{}"
                       onclick="document.getElementById('paypal-submit').submit(); return false;">
                        <i class="fa fa-heart"></i>&nbsp;&nbsp;Donate now</a>
                </div>
                <div class="hero-image">
                    <?php
                    echo do_shortcode("[metaslider id=97]");
                    ?>
                </div>
            </div>
            <div class="news-ticker">
                <div class="news-title">Charity News</div>
                <div class="latest-news">
                    <?php
                    // Get latest news title and permalink
                    $args = array('numberposts' => 1, 'cat' => '-4');
                    $recent_posts = wp_get_recent_posts($args);
                    echo '<a href="' . get_permalink($recent_posts[0]["ID"]) . '" class="white-link js--recent-news-text">' . $recent_posts[0]["post_title"] . '</a>';
                    wp_reset_query();
                    ?></div>
            </div>
<div id="amznCharityBanner"><script type="text/javascript">(function() {var iFrame = document.createElement('iframe'); iFrame.style.display = 'none'; iFrame.style.border = "none"; iFrame.width = 310; iFrame.height = 256; iFrame.setAttribute && iFrame.setAttribute('scrolling', 'no'); iFrame.setAttribute('frameborder', '0'); setTimeout(function() {var contents = (iFrame.contentWindow) ? iFrame.contentWindow : (iFrame.contentDocument.document) ? iFrame.contentDocument.document : iFrame.contentDocument; contents.document.open(); contents.document.write(decodeURIComponent("%3Cdiv%20id%3D%22amznCharityBannerInner%22%3E%3Ca%20href%3D%22https%3A%2F%2Fsmile.amazon.co.uk%2Fch%2FSC044899%22%20target%3D%22_blank%22%3E%3Cdiv%20class%3D%22text%22%20height%3D%22%22%3E%3Cdiv%20class%3D%22support-wrapper%22%3E%3Cdiv%20class%3D%22support%22%20style%3D%22font-size%3A%2025px%3B%20line-height%3A%2028px%3B%20margin-top%3A%201px%3B%20margin-bottom%3A%201px%3B%22%3ESupport%20%3Cspan%20id%3D%22charity-name%22%20style%3D%22display%3A%20inline-block%3B%22%3ESunrise%20Africa%20Relief%20Limited%3C%2Fspan%3E%3C%2Fdiv%3E%3C%2Fdiv%3E%3Cp%20class%3D%22when-shop%22%3EWhen%20you%20shop%20at%20%3Cb%3Esmile.amazon.co.uk%2C%3C%2Fb%3E%3C%2Fp%3E%3Cp%20class%3D%22donates%22%3EAmazon%20Donates%3C%2Fp%3E%3C%2Fdiv%3E%3C%2Fa%3E%3C%2Fdiv%3E%3Cstyle%3E%23amznCharityBannerInner%7Bbackground-image%3Aurl(https%3A%2F%2Fm.media-amazon.com%2Fimages%2FG%2F02%2Fx-locale%2Fpaladin%2Fcharitycentral%2Fbanner-background-image._CB485923047_.png)%3Bwidth%3A302px%3Bheight%3A250px%3Bposition%3Arelative%7D%23amznCharityBannerInner%20a%7Bdisplay%3Ablock%3Bwidth%3A100%25%3Bheight%3A100%25%3Bposition%3Arelative%3Bcolor%3A%23000%3Btext-decoration%3Anone%7D.text%7Bposition%3Aabsolute%3Btop%3A20px%3Bleft%3A15px%3Bright%3A15px%3Bbottom%3A100px%7D.support-wrapper%7Boverflow%3Ahidden%3Bmax-height%3A86px%7D.support%7Bfont-family%3AArial%2Csans%3Bfont-weight%3A700%3Bline-height%3A28px%3Bfont-size%3A25px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D.when-shop%7Bfont-family%3AArial%2Csans%3Bfont-size%3A15px%3Bfont-weight%3A400%3Bline-height%3A25px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D.donates%7Bfont-family%3AArial%2Csans%3Bfont-size%3A15px%3Bfont-weight%3A400%3Bline-height%3A21px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D%3C%2Fstyle%3E")); contents.document.close(); iFrame.style.display = 'block';}); document.getElementById('amznCharityBanner').appendChild(iFrame); })(); </script></div>

            <div class="featured-blog">
                <img class="blog-image"
                     src="<?php echo get_template_directory_uri(); ?>/assets/images/hands.jpg">

                <h2><span><a href="2018/08/20/volunteering-in-a-developing-country-help-or-hindrance/">Blog: Volunteering in a Developing Country</a></span> <span
                            class="blog-text-black"> by Catriona Valenta</span></h2>
                <p>A help or hindrance?</p>
            </div>
            <div class="box-container">
                <div class="box what-we-do">
                    <h1>Projects</h1>
                    <p>Find out more about Sunrise Africa Relief and our projects in Malawi, Uganda, South Africa and
                        Zambia.</p>
                    <a href="/projects" class="white-link">Find out more</a>
                </div>
                <div class="box get-involved">
                    <h1>Volunteer</h1>
                    <p>We are always looking for volunteers to help. Volunteering can help you gain new skills and meet
                        new people - you could really make a difference!</p>
                    <a href="/volunteer" class="white-link">Learn more</a>
                </div>
                <div class="box charity-shop">
                    <h1>About Us</h1>
                    <p>Find out more about Sunrise Africa Relief, our mission and how we began.</p>
                    <a href="/about" class="white-link">About Us</a>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
