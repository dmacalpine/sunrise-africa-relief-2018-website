<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 * Template Name: Contact Page
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sar
 */

get_header(); ?>

    <div id="primary" class="content-area contact-area">
        <main id="main" class="site-main container" role="main">
            <?php echo do_shortcode("[caldera_form id=\"CF5aaca2d58049b\"]"); ?>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php

get_footer();
