<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sar
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'sar'); ?></a>
    <header id="masthead" class="site-header container" role="banner">
        <div class="site-logo">
            <?php $site_title = get_bloginfo('name'); ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                <div class="screen-reader-text">
                    <?php printf(esc_html__('Go to the homepage of %1$s', 'sar'), $site_title) ?>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" width="170px"
                     alt="Sunrise Africa Relief logo">
            </a>
        </div><!-- .site-logo -->
        <nav id="site-navigation" class="main-navigation" role="navigation">
            <button class="menu-toggle" aria-controls="primary-menu"
                    aria-expanded="false"><?php esc_html_e('Primary Menu', 'sar'); ?></button>
            <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>
        </nav><!-- #site-navigation -->
        <div class="menu-bar">
            <div class="search-box">
<!--                --><?php //get_search_form(); ?><!--x-->
                <input type="search" value="' . get_search_query() . '" name="search" placeholder="Search&hellip;"/>
                <div class="icon"><a href="/contact" class="fa fa-envelope-o social-ico" target="_blank" rel="noopener noreferrer"></a></div>
                <div class="icon"><a href="https://www.instagram.com/robertwilliamson2121/" class="fa fa-instagram social-ico" target="_blank" rel="noopener noreferrer"></a></div>
                <div class="icon"><a href="https://en-gb.facebook.com/Sunrise-Africa-Relief-1537306919819893/" class="fa fa-facebook social-ico" target="_blank" rel="noopener noreferrer"></a></div>
                <div class="icon"><a href="https://twitter.com/sunrise_africa" class="fa fa-twitter social-ico" target="_blank" rel="noopener noreferrer"></a></div>
                <strong class="find-us">Stay in touch: </strong>
            </div>
        </div>
        <!--End Menu Bar / Social Bookmarks -->
    </header><!-- #masthead -->

    <div id="content" class="site-content">
