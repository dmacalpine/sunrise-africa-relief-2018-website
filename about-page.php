<?php /* Template Name: about-page */ ?>

<?php


get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main container" role="main">
            <h1><?php wp_title('', true, ''); ?><br></h1>
            <div class="about-text-l">

                <p>Sunrise Africa Relief is a registered Scottish charity based in Scotland's ancient capital,
                    Dunfermline.
                    The charity began in 2014 on the back of yearly interfaith tours to Africa when two families decided
                    to
                    start a charity in Scotland with different faith groups working together, making us unique in
                    Scotland.

                    Sunrise Africa Relief seeks a world of ethical action, social justice, self-reliance and
                    opportunity. We
                    hope to realize this vision by acting as a catalyst for global social change. Sunrise Africa Relief
                    creates opportunities for human development, community welfare and social progress through
                    educational
                    service projects, while endorsing the essential principles of compassion and cooperation as the
                    cornerstones of world peace. Read more about our projects.</p></div>
            <div class="about-text-r">
            </div>
            <div class="mission-statement">
                <h2>Mission Statement</h2>
                <p>Sunrise Africa Relief (SAR) is dedicated to the purpose of finding practical solutions to the
                    global problems of poverty, suffering and disease while promoting opportunities for long-term
                    sustainable development for communities in need.</p>
            </div>
<div class="charity-objectives">
    <h2>Charity Objectives</h2>
            <ol>
                <li>To promote the education of children in Africa, through schools and educational bodies</li>
                <li>To promote good health care through supporting clinics and first aid stations</li>
                <li>To relieve poverty and promote the welfare of orphans</li>
                <li>To work with and arrange to distribute funds through charities and community groups and faith based
                    organisations in Africa and the UK
                </li>
                <li>To support local philanthropy and transparent grant making within Africa and the UK</li>
            </ol></div>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
